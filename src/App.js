import React, { useState, createContext, useMemo } from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom';
import Header from './components/Header';
import Home from './components/Home';
import Footer from './components/Footer';
import Places from './components/Places';
import Sponsor from './components/Sponsor';
import Subscribe from './components/Subscribe';
import Scrollup from './components/Scrollup';
import Video from './components/Video';
import Experience from './components/Experience';
import Discover from './components/Discover';
import About from './components/About';

export const CountryContext = createContext();

export default function App() {
  const [state, setState] = useState({
    id: 1,
    country: "Hawaii",
    travel: false
  });

  return (
    <div className="App">
    <Router>
      <Switch>
        <Route exact path="/">
            <Header></Header>
            <main className="main">
            <CountryContext.Provider value={[state, setState]}>
              <Home></Home>
              <About></About>
              <Discover></Discover>
              <Experience></Experience>
              <Video></Video>
              <Places></Places>
              <Subscribe></Subscribe>
              <Sponsor></Sponsor>
            </CountryContext.Provider>
            </main>
            <Footer></Footer>
            <Scrollup></Scrollup>
        </Route>
      </Switch>
    </Router>
    </div>
  );
}
