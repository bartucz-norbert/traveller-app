import React from 'react'

export default function Sponsor() {
    return (
        <section className="sponsor section">
            <div className="sponsor__container container grid">
                <div className="sponsor__content">
                    <img src="/img/sponsors1.png" alt="" className="sponsor__img"/>
                </div>
                <div className="sponsor__content">
                    <img src="/img/sponsors2.png" alt="" className="sponsor__img"/>
                </div>
                <div className="sponsor__content">
                    <img src="/img/sponsors3.png" alt="" className="sponsor__img"/>
                </div>
                <div className="sponsor__content">
                    <img src="/img/sponsors4.png" alt="" className="sponsor__img"/>
                </div>
                <div className="sponsor__content">
                    <img src="/img/sponsors5.png" alt="" className="sponsor__img"/>
                </div>
            </div>
        </section>
    )
}
