import React, { useState } from 'react';

export default function Places() {
    const [places, setPlaces] = useState([
        {
            id: 1,
            image: "place1.jpg",
            rate : 4.8,
            title: "Bali",
            subtitle: "Indonesia",
            price: 2499,
        },   
        {
            id: 2,
            image: "place2.jpg",
            rate : 4.8,
            title: "Bora Bora",
            subtitle: "Polinesia",
            price: 1599
        },     
        {
            id: 3,
            image: "place3.jpg",
            rate : 4.9,
            title: "Hawaii",
            subtitle: "U.S.A",
            price: 3499
        },
        {
            id: 4,
            image: "place4.jpg",
            rate : 4.8,
            title: "Whitehaven",
            subtitle: "Australia",
            price: 2499
        },     
        {
            id: 5,
            image: "place5.jpg",
            rate : 3.9,
            title: "Hvar",
            subtitle: "Croacia",
            price: 1999
        }
    ]);

    return (
        <section className="place section" id="place">
            <h2 className="section__title">Choose Your Place</h2>
            <div className="place__container container grid">
                {places.map((place) => (
                    <div key={place.id} className="place__card">
                        <img src={'/img/' + place.image} alt="" className="place__img"/>
                        <div className="place__content">
                            <span className="place__rating">
                                <i className="ri-star-line place__rating-icon"></i>
                                <span className="place__rating-number">{place.rate}</span>
                            </span>
                            <div className="place__data">
                                <h3 className="place__title">{place.title}</h3>
                                <span className="place__subtitle">{place.subtitle}</span>
                                <span className="place__price">{'$' + place.price}</span>
                            </div>
                        </div>
                        <button className="button button--flex place__button"><i className="ri-arrow-right-line"></i></button>
                    </div>
                ))}
            </div>
        </section>
    )
}
