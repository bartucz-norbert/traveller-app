import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Scrollup() {
    const [offset, setOffset] = useState(0);

    useEffect(() => {
      window.onscroll = () => {
        setOffset(window.pageYOffset)
      }
    }, []);

    return (
        <span className={offset >= 200 ? 'scrollup show-scroll' : 'scrollup'} id="scroll-up">
            <i className="ri-arrow-up-line scrollup__icon"></i>
        </span>
    )
}
