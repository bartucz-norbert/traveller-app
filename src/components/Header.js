import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function Header() {
    const [navState, setNavState] = useState(false);
    const [offset, setOffset] = useState(0);

    const navToggle = () => {
        return setNavState(!navState);
    };

    useEffect(() => {
      window.onscroll = () => {
        setOffset(window.pageYOffset)
      }
    }, []);

    return (
        <div className={offset >= 100 ? 'header' : 'header scroll-header'}>
            <nav className="nav container">
                <Link to="/" className="nav__logo">Travel</Link>
                <div className={navState ? 'nav__menu show-menu' : 'nav__menu'} id="nav-menu">
                    <ul className="nav__list">
                        <li className="nav__item">
                            <Link to="/home" className="nav__link">Home</Link>
                        </li>       
                        <li className="nav__item">
                            <Link to="/about" className="nav__link">About</Link>
                        </li>        
                        <li className="nav__item">
                            <Link to="/discover" className="nav__link">Discover</Link>
                        </li>       
                        <li className="nav__item">
                            <Link to="/place" className="nav__link">Place</Link>
                        </li>
                        <i className="ri-close-line nav__close" id="nav-close"></i>     
                    </ul>

                    <div className="nav__dark">
                        <span className="change-theme-name">Dark mode</span>
                        <i className="ri-moon-line change-theme" id="theme-button"></i>
                    </div>

                    <i className="ri-close-line nav__close" onClick={navToggle}></i>
                </div>

                <div className="nav__toggle" onClick={navToggle}>
                    <i className="ri-function-line"></i>
                </div>
            </nav>
        </div>
    )
}
