import React, { useContext } from 'react';
import { CountryContext } from '../App';
import { Link } from 'react-router-dom';

export default function Home() {
    const [value, setValue] = useContext(CountryContext);

    return (
        <section className="home" id="home">
            <img src="/img/home1.jpg" alt="" className="home__img"/>

            <div className="home__container container grid">
                <div className="home__data">
                    <span className="home__data-subtitle">Discover your place</span>
                    <h1 className="home__data-title">Explore The <br/> Best <span style={{fontWeight: "bold"}}> Beautiful <br/> beaches </span></h1>
                    <Link to="#" className="button">Explore {value.id}</Link>  

                    <button className="button" onClick={() => setValue(() => ({...value,id: value.id++}))}>sdasdasd</button>             
                </div>

                <div className="home__social">
                    <Link to="#" target="_blank" className="home__social-link">
                        <i className="ri-facebook-box-fill"></i>
                    </Link>       
                    <Link to="#" target="_blank" className="home__social-link">
                        <i className="ri-instagram-fill"></i>
                    </Link>       
                    <Link to="#" target="_blank" className="home__social-link">
                        <i className="ri-twitter-box-fill"></i>
                    </Link>
                </div>

                <div className="home__info">
                    <div>
                        <span className="home__info-title">5 best places to visit</span>
                        <Link to="#" className="button button--flex button--link home__info-button">
                            More <i className="ri-arrow-right-line"></i>
                        </Link>
                    </div>
                    <div className="home__info-overlay">
                        <img src="/img/home2.jpg" alt="" className="home__info-img"/>
                    </div>
                </div>
            </div>
        </section>
    )
}
