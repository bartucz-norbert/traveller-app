import React from 'react';
import { Link } from 'react-router-dom';

export default function Footer() {
    return (
        <footer className="footer section">
          <div className="footer__container container grid">
            <div className="footer__content grid">
              <div className="footer__data">
                <h3 className="footer__title">Travel</h3>
                <p className="footer__description">Travel you choose the <br/> destination, we offer you the <br/> experience.</p>
        
                <div>
                  <Link to="https://www.facebook.com" target="_blank" className="footer__social">
                    <i className="ri-facebook-box-fill"></i>
                  </Link>       
                  <Link to="https://www.twitter.com" target="_blank" className="footer__social">
                    <i className="ri-twitter-fill"></i>
                  </Link>          
                  <Link to="https://www.instagram.com" target="_blank" className="footer__social">
                    <i className="ri-instagram-fill"></i>
                  </Link>         
                  <Link to="https://www.youtube.com" target="_blank" className="footer__social">
                    <i className="ri-youtube-fill"></i>
                  </Link>
                </div>
              </div>
        
              <div className="footer__data">
                <h3 className="footer__subtitle">About</h3>
        
                <ul>
                  <li className="footer__item">
                    <Link to="#" className="footer__link">About Us</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Features</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">New &amp; Blog</Link>
                  </li>
                </ul>
        
              </div>
        
              <div className="footer__data">
                <h3 className="footer__subtitle">Company</h3>
                <ul>
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Team</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Plan y Pricing</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Become a member</Link>
                  </li>
                </ul>
              </div>
        
              <div className="footer__data">
                <h3 className="footer__subtitle">Support</h3>
                <ul>
                  <li className="footer__item">
                    <Link to="#" className="footer__link">FAQs</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Support Center</Link>
                  </li>          
                  <li className="footer__item">
                    <Link to="#" className="footer__link">Contact us</Link>
                  </li>
                </ul>
              </div>
            </div>
        
            <div className="footer__rights">
              <p className="footer__copy">&#169; 2021 Bartucz. All rights reserved.</p>
              <div className="footer__terms">
                <Link to="#" className="footer__terms-link">Terms &amp; Agreements</Link>
                <Link to="#" className="footer__terms-link">Privacy Policy</Link>
              </div>
            </div>
    
          </div>
        </footer>
    )
}
