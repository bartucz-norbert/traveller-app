import React, { useState } from 'react';
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/swiper.min.css";

export default function Discover() {
    const [discovers, setDiscovers] = useState([
        {
            id: 1,
            image: "discover1.jpg",
            title: "Bali",
            description: "24 tours available"
        },    
        {
            id: 2,
            image: "discover2.jpg",
            title: "Hawaii",
            description: "15 tours available"
        },      
        {
            id: 3,
            image: "discover3.jpg",
            title: "Hvar",
            description: "14 tours available"
        },  
        {
            id: 4,
            image: "discover4.jpg",
            title: "Whitehaven",
            description: "32 tours available"
        }
    ]);

    return (
    <section className="discover section" id="discover">
        <h2 className="section__title">Discover the most <br/> attractive places</h2>
        
        <div className="discover__container container swiper-container">
            <div className="swiper-wrapper">
                <Swiper
                    spaceBetween={5}
                    slidesPerView={3}
                    navigation
                    pagination={{ clickable: true }}
                    scrollbar={{ draggable: true }}
                    onSlideChange={() => console.log('slide change')}
                    onSwiper={(swiper) => console.log(swiper)}
                >
                    {discovers.map((discover) => (
                        <SwiperSlide key={discover.id}>
                            <div className="discover__card swiper-slide">
                                <img src={'/img/' + discover.image} alt="" className="discover__img"/>
                                <div className="discover__data">
                                    <h2 className="discover__title">
                                        {discover.title}
                                    </h2>
                                    <span className="discover__description">
                                        {discover.description}
                                    </span>
                                </div>
                            </div>
                        </SwiperSlide>
                    ))}
                </Swiper>
            </div>
        </div>
    </section>
    )
}
